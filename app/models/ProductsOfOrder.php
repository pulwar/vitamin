<?php

class ProductsOfOrder extends \Eloquent {

	protected $table = 'order_product';

	// Add your validation rules here
	public static $rules = [
		//'name' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['order_id','product_id','wash','clean','count'];

	public function product()
	{
		return $this->belongsTo('Product');
	}
	
}