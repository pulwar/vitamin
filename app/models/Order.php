<?php

class Order extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'name' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['name','phone','paid','address','price','orderStatus_id','status','client_id','hash','description'];

	public function products()
	{
		return $this->belongsToMany('Product');
	}

	public function status()
	{
		return $this->belongsTo('OrderStatus','orderStatus_id');
	}

	public function client()
	{
		return $this->belongsTo('Client','client_id');
	}
}