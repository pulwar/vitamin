<?php

class Promo extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'code' => 'required',
		'value' => 'required',
	];

	// Don't forget to fill this array
	protected $fillable = ['code','value','flagSingleUser','valueType','dateStart','dateEnd','notWork'];

}