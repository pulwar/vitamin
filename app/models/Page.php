<?php

class Page extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'name' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['name','url','description','description_two','description_three','description_four','description_five','substrate'];

}