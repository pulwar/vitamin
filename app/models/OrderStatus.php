<?php

class OrderStatus extends \Eloquent {

	protected $table = 'orderstatuses';
	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['name'];	

	public function orders()
	{
		return $this->hasMany('Order');
	}
}