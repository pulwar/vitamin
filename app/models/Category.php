<?php

class Category extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'name' => 'required',
		'description' => 'required',
	];

	// Don't forget to fill this array
	protected $fillable = ['name','description','products_count'];

	public function products()
	{
		return $this->hasMany('Product')->orderBy('order','asc');
	}

} 