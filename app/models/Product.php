<?php
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Product extends \Eloquent implements StaplerableInterface {

 	use EloquentTrait;

	// Add your validation rules here
	public static $rules = [
		'name' => 'required',
		'description' => 'required',
	];

	// Don't forget to fill this array
	protected $fillable = ['name','description','packing','category_id','price','oldPrice','shortName','img','status_id','unit','step','wash_flag','hide'];

	public function category()
	{
		return $this->belongsTo('Category');
	}

	public function status()
	{
		return $this->belongsTo('Status');
	}

	public function orders()
	{
		return $this->belongsToMany('Order');
	}

	public function productsOfOrder()
	{
		return $this->hasMany('ProductOfOrder');
	}

	public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('img', [
            'styles' => [           
            'thumb' => '264x205'
            ]
        ]);

        parent::__construct($attributes);
    }
}