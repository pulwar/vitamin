<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
/*
Route::group(array('prefix' => 'admin'), function()
{
	Route::get('login', array('as' => 'admin.login', 'uses' => 'AdminAuthController@getLogin'));
	Route::post('post', array('as' => 'admin.login.post', 'uses' => 'AdminAuthController@postLogin'));
	Route::get('logout', array('as' => 'admin.logout', 'uses' => 'AdminAuthController@getLogout'));
});	
*/
Route::get('admin.categories.index', [
'before' => "logged:/custom_url",
'uses' => function(){
     echo '123'; die();
}]);
Route::group(array('prefix' => 'admin', /*[
										'before' => 'has_perm:_permissionA,_permissionB',    									
        								'uses' => 'Jacopo\Authentication\Controllers\UserController@getList']*/
					), function(){

							Route::resource('categories', 'AdminCategoriesController',array('only' => array('index','edit','update')));	
							Route::resource('products', 'AdminProductsController',array('only' => array('index','edit','create','store','destroy')));	
							Route::post('products/update/{products}',array('as' => 'admin.products.update', 'uses' => 'AdminProductsController@update'));	

							Route::resource('orders','AdminOrdersController');
							
							Route::resource('pof','AdminProductsOfOrdersController', array('except' => array('create')));

							Route::get('admin/pof/{orderId}/create',array('as' => 'admin.pof.create','uses' => 'AdminProductsOfOrdersController@create',array('only' => array('create'))));

							Route::resource('pages', 'AdminPagesController');

							Route::resource('clients', 'AdminClientsController');

							Route::resource('promos','AdminPromosController');

							Route::resource('settings','AdminSettingsController');
							
							//порядок продуктов
							Route::post('products/order-update',array('as' => 'ord-update', 'uses' => 'AdminCategoriesController@changeOrderOfProduct'));	
							//тестовый
							/*
							Route::get('mail',function(){
								Mail::send('admin.email.hello',array('name' => 'test'), function($message){
									$message->to('ma@helloworld.by', 'Test')->subject('Hello, Are you heare me?');
									echo 123;
								});
							});
							*/
		  
		});


//Route::resource('categories', 'AdminCategoriesController',array('only' => array('index','edit','update')));	


Route::resource('/','BaseController');
//add prod to session
Route::post('add-product',array('as' => 'add.pdoduct', 'uses' => 'BaseController@addProdToCart'));
Route::post('show-cart',array('as' => 'show.cart', 'uses' => 'BaseController@showCart'));
Route::post('del-from-cart',array('as' => 'del.from.cart', 'uses' => 'BaseController@delFromCart'));
Route::post('save-order',array('as' => 'save.order', 'uses' => 'BaseController@saveOrder'));
Route::post('check-promo',array('as' => 'check.promo', 'uses' => 'BaseController@checkPromo'));
Route::post('check-address',array('as' => 'check.address', 'uses' => 'BaseController@checkAddress'));
//страница оплаты заказа
Route::get('payment/{hash}',array('as' => 'order.payment', 'uses' => 'BaseController@payment'));	
//страница удачной или не удачной оплаты
Route::post('payment',array('as' => 'order.payment.result', 'uses' => 'BaseController@paymentResult'));	
//приход платежа
Route::post('payment_came',array('as' => 'order.payment.came', 'uses' => 'BaseController@paymentCame'));
//статические страницы
Route::get('{name}',array('as' => 'static.page', 'uses' => 'BaseController@staticPage'))
->where('name', '[A-Za-z]+');

