<?php

class AdminOrdersController extends \BaseController {

	/**
	 * Display a listing of orders
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Input::all();		
		isset($data['fromDate']) ? $fromDate = $data['fromDate'] : $fromDate = date('Y-m-d');
		isset($data['status']) ? $status = $data['status'] : $status = 0;
		isset($data['find']) ? $find = $data['find'] : $find = null;
		
		if(isset($data['underDate'])){
			$underDate = $data['underDate'];
		} else {
			$firstOrder = Order::select('orders.created_at')->orderBy('orders.created_at','asc')->first();		
			$firstData  = $firstOrder->created_at->format("Y-m-d");
			$underDate = $firstData;
		}  
		
		$qb = Order::select('orders.*')
					->where('orders.created_at', '>=',$underDate.' 00:00')
					->where('orders.created_at', '<=',$fromDate.' 23:59')
					->orderBy('orders.created_at','desc');
		if($status){
			$qb ->where('orders.orderStatus_id','=',$status);
		}
		if($find){
			$qb->where('orders.phone', '=',$find)
          		//->orWhere('orders.email', 'LIKE', '%'.$find.'%')
          		->orWhere('orders.id', '=', $find);
		}			
		$orders = $qb->get();			
		return View::make('admin.orders.index', compact('orders','underDate','fromDate','status','find'));
	}

	/**
	 * Show the form for creating a new order
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.orders.create');
	}

	/**
	 * Store a newly created order in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Order::$rules);
		
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$order = Order::create($data);
		
		//ищем клиента
		$client = Client::where('phone','=',$data['phone'])->first();

		//если нет - создаем нового
		if(count($client)!=1){
			$client = new Client;
			$client->save();
		}

		//меняем лд
		$client->name = $data['name'];
		$client->phone = $data['phone'];
		$client->address = $data['address'];
		$client->save();

		$order->client_id = $client->id;
		$order->hash = md5(rand());
		$order->save();

		return Redirect::route('admin.orders.show',array($order->id));
	}

	/**
	 * Display the specified order.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$order = Order::with('status')->findOrFail($id);
		//echo "<pre>";
		//var_export($order); die();
		$productsOfOrder = ProductsOfOrder::where('order_id','=',$id)->get();		

		return View::make('admin.orders.show', compact('order','productsOfOrder'));
	}

	/**
	 * Show the form for editing the specified order.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$order = Order::find($id);

		return View::make('admin.orders.edit', compact('order'));
	}

	/**
	 * Update the specified order in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$order = Order::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Order::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$order->update($data);

		return Redirect::route('admin.orders.show',array($id));
	}

	/**
	 * Remove the specified order from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Order::destroy($id);

		return Redirect::route('admin.orders.index');
	}

}
