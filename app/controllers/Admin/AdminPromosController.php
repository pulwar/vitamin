<?php

class AdminPromosController extends \BaseController {

	/**
	 * Display a listing of promos
	 *
	 * @return Response
	 */
	public function index()
	{
		$promos = Promo::all();

		return View::make('admin.promos.index', compact('promos'));
	}

	/**
	 * Show the form for creating a new promo
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.promos.create');
	}

	/**
	 * Store a newly created promo in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Promo::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Promo::create($data);

		return Redirect::route('admin.promos.index');
	}

	/**
	 * Display the specified promo.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$promo = Promo::findOrFail($id);

		return View::make('admin.promos.show', compact('promo'));
	}

	/**
	 * Show the form for editing the specified promo.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$promo = Promo::find($id);

		return View::make('admin.promos.edit', compact('promo'));
	}

	/**
	 * Update the specified promo in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$promo = Promo::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Promo::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$promo->update($data);

		return Redirect::route('admin.promos.index');
	}

	/**
	 * Remove the specified promo from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Promo::destroy($id);

		return Redirect::route('admin.promos.index');
	}

}
