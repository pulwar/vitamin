<?php

class AdminClientsController extends \BaseController {

	/**
	 * Display a listing of clients
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Input::all();
		isset($data['sortingBy']) ? $sortingBy = $data['sortingBy'] : $sortingBy = 'clients.name';
		isset($data['sortingAs']) ? $sortingAs = $data['sortingAs'] : $sortingAs = 'asc';
		isset($data['find']) ? $find = $data['find'] : $find = null;

		$qb = Client::with('orders')
						 	->join('orders','orders.client_id','=','clients.id')
        					->select('clients.*',DB::raw('SUM(`orders`.`price`)'))
				            ->groupBy('clients.id')				            
				            ->orderBy(DB::raw($sortingBy), $sortingAs);

		if($find){
			$qb->where('clients.phone', 'LIKE','%'.$find.'%')
          		->orWhere('clients.email', 'LIKE', '%'.$find.'%');
		}

		$clients = $qb->get();

		return View::make('admin.clients.index', compact('clients','sortingBy','sortingAs'));
	}

	/**
	 * Show the form for creating a new client
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.clients.create');
	}

	/**
	 * Store a newly created client in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Client::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Client::create($data);

		return Redirect::route('admin.clients.index');
	}

	/**
	 * Display the specified client.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$client = Client::findOrFail($id);

		return View::make('admin.clients.show', compact('client'));
	}

	/**
	 * Show the form for editing the specified client.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$client = Client::find($id);

		return View::make('admin.clients.edit', compact('client'));
	}

	/**
	 * Update the specified client in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$client = Client::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Client::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$client->update($data);

		return Redirect::route('admin.clients.index');
	}

	/**
	 * Remove the specified client from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Client::destroy($id);

		return Redirect::route('admin.clients.index');
	}

}
