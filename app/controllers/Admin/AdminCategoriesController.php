<?php

class AdminCategoriesController extends \BaseController {

	/**
	 * Display a listing of categories
	 *
	 * @return Response
	 */
	public function index()
	{
		$categories = Category::with('products')->get();

		return View::make('admin.categories.index', compact('categories'));
	}

	/**
	 * Show the form for editing the specified category.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = Category::find($id);

		return View::make('admin.categories.edit', compact('category'));
	}

	/**
	 * Update the specified category in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$category = Category::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Category::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$category->update($data);

		return Redirect::route('admin.categories.index');
	}	

	public function changeOrderOfProduct()
	{
		if (isset($_POST['arr'])){
			$i = 1;
			foreach ($_POST['arr'] as $value) {
				$product = Product::find($value);
				$product->order = $i;
				$product->save();
				$i++;
			}
			$result = 1;
		} else {
			$result = 0;
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

}	
