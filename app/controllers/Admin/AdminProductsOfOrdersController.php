<?php

class AdminProductsOfOrdersController extends \BaseController {
	

	/**
	 * Show the form for creating a new productsoforder
	 *
	 * @return Response
	 */
	public function create($orderId)
	{		
		$oId = $orderId;
		return View::make('admin.productsoforders.create', compact('oId'));
	}

	/**
	 * Store a newly created productsoforder in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), ProductsOfOrder::$rules);
		
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		ProductsOfOrder::create($data);

		return Redirect::route('admin.orders.show',array($data['order_id']));
	}	

	/**
	 * Show the form for editing the specified productsoforder.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$productsoforder = ProductsOfOrder::find($id);

		return View::make('admin.productsoforders.edit', compact('productsoforder'));
	}

	/**
	 * Update the specified productsoforder in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$productsoforder = ProductsOfOrder::findOrFail($id);

		$validator = Validator::make($data = Input::all(), ProductsOfOrder::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$productsoforder->update($data);

		return Redirect::route('admin.orders.show',array($productsoforder->order_id));
	}

	/**
	 * Remove the specified productsoforder from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	$productsoforder = ProductsOfOrder::findOrFail($id);
		$orderId = $productsoforder->order_id;
		ProductsOfOrder::destroy($id);

		return Redirect::route('admin.orders.show',array($orderId));
	}

}
