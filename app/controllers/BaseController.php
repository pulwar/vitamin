<?php

class BaseController extends Controller {	
	
	//главная корзина
	public function index()
	{
		
		if(!isset($_SESSION)) {
			session_start();
		}
		//var_export($_SESSION);
		
		//session_destroy();		
		
		//cart
		if (isset($_SESSION)){
			if(isset($_SESSION['products_in_cart'])){
				$productsInCart = null;
				foreach ($_SESSION['products_in_cart'] as $key => $product) {
					$productsInCart[$key] = $product['count'];					 
				}

			}else{
				$productsInCart = null;
			}
		} else{
			$productsInCart = null;
		}
		
		$categories = Category::with('products')->get();

		$products = Product::all();

		$freeDelivery = Setting::where('name','=','freeDelivery')->first()->value;

		$delivery = Setting::where('name','=','delivery')->first()->value;

		return View::make('frontend.index', compact('categories','productsInCart','products','freeDelivery','delivery'));
	}	

	//страница оплаты заказа
	public function payment($hash)
	{
		$data = Input::all();
		if(isset($data['success'])){		
			$data['success']==1 ? $success = 1 : $success = 0; 
		} else{
			$success = 3;
		}	

		$order = Order::where('hash','=',$hash)->first();

		if($order){
			$mrh_login = "vitaminchik";
			$mrh_pass1 = "pulwar123";

			// номер заказа
			// number of order
			$inv_id = $order->id;

			// описание заказа
			// order description
			$inv_desc = "ROBOKASSA Advanced User Guide";

			// сумма заказа
			// sum of order
			$out_summ = $order->price;

			// тип товара
			// code of goods
			$shp_item = "2";

			// предлагаемая валюта платежа
			// default payment e-currency
			$in_curr = "";

			// язык
			// language
			$culture = "ru";

			// формирование подписи
			// generate signature
			$crc  = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item");

			// форма оплаты товара
			// payment form
			$form =  
			      "<form action='http://test.robokassa.ru/Index.aspx' method=POST>".
			      "<input type=hidden name=MrchLogin value=$mrh_login>".
			      "<input type=hidden name=OutSum value=$out_summ>".
			      "<input type=hidden name=InvId value=$inv_id>".
			      "<input type=hidden name=Desc value='$inv_desc'>".
			      "<input type=hidden name=SignatureValue value=$crc>".
			      "<input type=hidden name=Shp_item value='$shp_item'>".
			      "<input type=hidden name=IncCurrLabel value=$in_curr>".
			      "<input type=hidden name=Culture value=$culture>".
			      "<input type=submit value='Pay'>".
			      "</form>";
      	}
		return View::make('frontend.payment', compact('order','form','success'));
	}

	public function paymentResult(){
		$data = Input::all();
		if($data['InvId']){
			$orderHash = Order::where('id','=',$data['InvId'])->first()->hash;
			if($data['ok']=='true'){
				$success = true;
			}else{
				$success = false;
			}
			return Redirect::route('order.payment',array($orderHash,'success'=>$success));	
		}

	}

	public function paymentCame(){
		$data = Input::all();		
		if($data["InvId"]){			
			// registration info (password #2)
			$mrh_pass2 = "pulwar321";

			
			//current date
			$tm=getdate(time()+9*3600);
			$date="$tm[year]-$tm[mon]-$tm[mday] $tm[hours]:$tm[minutes]:$tm[seconds]";

			
			// read parameters
			$out_summ = $data["OutSum"];
			$inv_id = $data["InvId"];
			$shp_item = $data["Shp_item"];
			$crc = $data["SignatureValue"];

			$crc = strtoupper($crc);

			$my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass2:Shp_item=$shp_item"));
			
			// check signature
			if ($my_crc !=$crc)
			{
			  echo "bad sign\n";
			  exit();
			}
			
			// success
			echo "OK$inv_id\n";
			
			//change order paid
			$order = Order::where('id','=',$data['InvId'])->first();
			$order->paid = $out_summ;
			$order->save();

			// save order info to file
			$f=@fopen("order.txt","a+") or
			          die("error");
			fputs($f,"order_num :$inv_id;Summ :$out_summ;Date :$date\n");
			fclose($f);
		}
	}

	//добавляем/меняем продукт в корзину
	public function addProdToCart()
	{		
		if(!isset($_SESSION)) {
			session_start();
		}
		if(isset($_POST['wash'])){
			$wash = $_POST['wash'];
		} else if (isset($_SESSION['products_in_cart'][$_POST['productId']])){
			$wash = $_SESSION['products_in_cart'][$_POST['productId']]['wash'];
		} else {
			$wash = 0;
		} 

		//session_destroy();
		$_SESSION['products_in_cart'][$_POST['productId']] = array('count' => $_POST['count'], 'price' => $_POST['price'], 'wash' => $wash);
		//var_export($_SESSION);
		$result['count'] = count($_SESSION['products_in_cart']);
		$price = 0;
		foreach ($_SESSION['products_in_cart'] as $product) {
			$price += $product['price'];
		}
		$result['price'] = $price;	
		$result['cart'] = $_SESSION['products_in_cart'];	
		
		header('Content-Type: application/json');
		echo json_encode($result);	
	}

	//возвращаем данные корзины
	public function showCart()
	{
		if(!isset($_SESSION)) {
			session_start();
		}
		if(isset($_SESSION['products_in_cart'])){
			$result = $_SESSION['products_in_cart'];
		}else {
			$result = null;
		}
		if(isset($_SESSION['disc'])){
			unset($_SESSION['disc']);
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	//удаляем продукт из корзины
	public function delFromCart()
	{
		if(!isset($_SESSION)) {
			session_start();
		}
		$productId = $_POST['productId'];
		if($productId){
			unset($_SESSION['products_in_cart'][$productId]);	
		}
		
		if(isset($_SESSION['products_in_cart'])){

			$result['cart'] = $_SESSION['products_in_cart'];
		}else {
			$result = null;
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	//сохраняем заказ в БД
	public function saveOrder()
	{
		if(!isset($_SESSION)) {
			session_start();
		}
		if (isset($_POST['cart']) and isset($_SESSION['products_in_cart'])){
			$user = $_POST['cart'];

			//ищем клиента
			$client = Client::where('phone','=',$user['phone'])->first();

			//если нет - создаем нового
			if(count($client)!=1){
				$client = new Client;
				$client->save();
			}

			//меняем лд
			$client->name = $user['name'];
			$client->phone = $user['phone'];
			$client->address = $user['street'];
			$client->save();

			//сохраняем новый заказ
			$order = new Order;
			$order->name = $user['name'];
			$order->phone = $user['phone'];
			$order->address = $user['street'];
			$order->orderStatus_id = 1;//принят
			$order->hash = md5(rand());
			$order->client_id = $client->id;
			$order->save();

			//добавляем товары из сессии к заказу
			$price = 0;
			foreach ($_SESSION['products_in_cart'] as $key => $product) {
				$price += $product['price'];
				$pOfO = new ProductsOfOrder;
				$pOfO->order_id = $order->id;
				$pOfO->product_id = $key;
				if($product['wash']){
					$pOfO->wash = 1;
				}else{
					$pOfO->wash = 0;
				}
				$pOfO->count = $product['count'];
				$pOfO->save();
			}
			if(isset($_SESSION['disc'])){
				$discV = 0;
				if($_SESSION['disc']['type']='%'){			
					$discV = $price/100*$_SESSION['disc']['val'];
				} else {
					$discV = $_SESSION['disc']['val'];
				}
				$order->price = $price - $_SESSION['disc']['val'];
				$order->description = 'Скидка '.$_SESSION['disc']['val'].' '.$_SESSION['disc']['type'].'. Промо(id) = '.$_SESSION['disc']['promoId'];
			} else {
				$order->price = $price;	
			} 
			
			$order->save();
			

			session_destroy();//чистим сессии	

			$result = 1;

		}else{
			$result = 0;
		}
		

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function staticPage($name)
	{
		$page = Page::where('url','=',$name)->first();
		if(!count($page)){
			App::abort(404);
		}
		//выбор подложки
		if($page->substrate == 5){
			return View::make('frontend._layouts.substrate_five', compact('page'));
		}else if($page->substrate == 2){
			return View::make('frontend._layouts.substrate_two', compact('page'));
		}else{
			return View::make('frontend._layouts.substrate_one', compact('page'));
		}	
		
	}

	public function checkPromo()
	{
		$data = Input::all();
		if($data['promo']){
			if(!isset($_SESSION)) {
				session_start();
			}
			$promo = Promo::where('code','=',$data['promo'])->first();
			if($promo){
				$_SESSION['disc']['val'] = $promo->value;
				$_SESSION['disc']['type'] = $promo->valueType;
				$_SESSION['disc']['promoId'] = $promo->id;
				$result['disc'] = $_SESSION['disc'];
			}
		}
		//var_export($data); die();
		

		
		$result['cart'] = $_SESSION['products_in_cart'];

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function checkAddress()
	{
		$data = Input::all();
		
		//получаем координаты		

		$params = array(
		    'geocode' => 'Москва, ул. '.$data['street'].', '.$data['house'].'', // адрес
		    'format'  => 'json',                          // формат ответа
		    'results' => 1,                               // количество выводимых результатов
		                               
		);
		$response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));
 
		if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0)
		{
		    $point = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
			
		}
		else
		{
		    $point = 0;		    
		}

		//ищем в об-ти
		if($point){
			$points = explode(" ", $point);
			$city = DB::table('settings')
				//->whereRaw("ST_Within(GeometryFromText('POINT(".$points[1]." ".$points[0].")',0), GeometryFromText(settings.border,0))")
				->whereRaw("Within(GeometryFromText('POINT(".$points[1]." ".$points[0].")',0), GeometryFromText(settings.border,0))")
				->get();
				//echo count($city).'!!!!!';
			if(count($city)>0){
				$result = 1;		
			} else {
				$result = 0;
			}
		} else {
			$result = 0;
		}
		
		header('Content-Type: application/json');
		echo json_encode($result);
	}
}
