<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <title>ВИТАМИНЫ</title>
        <meta name="description" content="">
        <meta name="author" content="admin">

        <!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
        <link rel="shortcut icon" href="images/favicon.ico">
        <link rel="apple-touch-icon" href="images/favicon.png">
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/cusel.css" />
        
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/cusel-min-2.5.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function(){
                var params = {
                        changedEl: "select"
                    }
                cuSel(params);
            })
        </script>
        
        <!--[if lt IE 9]>
         <script type="text/javascript">
          var e = ("article,aside,figcaption,figure,footer,header,hgroup,nav,section,time").split(',');
          for (var i = 0; i < e.length; i++) {document.createElement(e[i]);}
         </script>
        <![endif]-->
    </head>

    <body class="body-lg">
        
        <div id="page">
            
            <header class="header-v2">
                <div id="header">
                    <div class="header-top">
                        <div class="header-top-inr">
                            <a href="#" id="logo"><img src="images/logo-2.png" alt="" /></a>
                            <nav>
                                <ul>
                                    <li><a href="#">ФРУКТЫ</a></li>
                                    <li><a href="#">СЕТЫ</a></li>
                                    <li><a href="#">ЯГОДЫ</a></li>
                                    <li><a href="#">ОВОЩИ</a></li>
                                    <li><a href="#">СУХОФРУКТЫ</a></li>
                                </ul>
                            </nav>
                            <div class="phone">
                                <p>8<i></i>800<i></i>345<i></i>0000</p>
                                <div>Круглосуточная доставка свежижих фруктов и овощей по Москве</div>
                            </div>
                        </div>
                    </div>
                    <div class="bg-center">
                        <img src="images/h-3.png" alt="" />
                    </div>
                </div>
            </header>
            
            <div id="content" class="bg-l">
                
                <div class="legend">
                    <div class="text text-1">
                        <div>
                            {{$page->description}}
                        </div>
                    </div>
                    <div class="text text-2">
                        <div>
                            {{$page->description_two}}
                        </div>
                    </div>
                    <div class="text text-3">
                        <div>
                            {{$page->description_three}}
                        </div>
                    </div>
                    <div class="text text-4">
                        <div>
                            {{$page->description_four}}
                        </div>
                    </div>
                    <div class="text text-5">
                        <div>
                            {{$page->description_five}}
                        </div>
                    </div>
                    
                    <div class="img-g-1"></div>
                    <div class="img-g-2"></div>
                    <div class="img-g-3"></div>
                    <div class="img-g-4"></div>
                    <div class="img-g-5"></div>
                </div>
                
            </div>

        </div>   
            
        <footer>
            <div class="footer-inr1">
                <ul>
                    <li class="active">
                        <a href="#">
                            <div class="image"><img src="images/footer-im1a.png" alt="" /></div>
                            <div class="title">ЛЕГЕНДА</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="image"><img src="images/footer-im2.png" alt="" /></div>
                            <div class="title">Наша Команда</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="image"><img src="images/footer-im3.png" alt="" /></div>
                            <div class="title">Доставка</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="image"><img src="images/footer-im4.png" alt="" /></div>
                            <div class="title">КОРПОРАТИВНЫМ КЛИЕНТАМ</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="footer-inr2">
                <div class="copy">© Все права защищены ООО «Вита Мин»</div>
                <div class="dev"><a href="#"></a></div>
                <div class="soc">
                    <div>Давайте дружить!</div>
                    <a href="#" class="vk"></a>
                    <a href="#" class="fa"></a>
                    <a href="#" class="tw"></a>
                    <a href="#" class="go"></a>
                </div>
            </div>
        </footer>
        
        <div class="fixes-s"></div>      
        
    </body>
</html>
