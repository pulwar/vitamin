<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <title>ВИТАМИНЫ</title>
        <meta name="description" content="">
        <meta name="author" content="admin">

        <!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
        <link rel="shortcut icon" href="images/favicon.ico">
        <link rel="apple-touch-icon" href="images/favicon.png">
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/cusel.css" />
        
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/cusel-min-2.5.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function(){
                var params = {
                        changedEl: "select"
                    }
                cuSel(params);
            })
        </script>
        
        <!--[if lt IE 9]>
         <script type="text/javascript">
          var e = ("article,aside,figcaption,figure,footer,header,hgroup,nav,section,time").split(',');
          for (var i = 0; i < e.length; i++) {document.createElement(e[i]);}
         </script>
        <![endif]-->
    </head>

    <body>
        
        <div id="page">
            
            <header>
                <div id="header">
                    <div class="header-inr1 clearfix">
                        <a href="#" id="logo"><img src="images/logo.png" alt="" /></a>
                        <div class="phone">
                            <p>8<i></i>800<i></i>345<i></i>0000</p>
                            <div>Круглосуточная доставка свежижих фруктов и овощей по Москве</div>
                        </div>
                    </div>
                    <div class="header-inr2">
                        <img src="images/img-1.png" alt="" />
                        <a href="#" class="btn-catalog">ПОСМОТРЕТЬ ПРИЛАВОК</a>
                    </div>
                    <nav id="top-nav">
                        <div class="image-top"></div>
                        <ul>
                            <li class="link-1"><a href="#bb1"><span class="image"></span><span class="text">Фруктовые сеты</span></a></li>
                            <li class="link-2"><a href="#bb2"><span class="image"></span><span class="text">Свежие Фрукты</span></a></li>
                            <li class="link-3"><a href="#bb3"><span class="image"></span><span class="text">полезные овощи</span></a></li>
                            <li class="link-4"><a href="#bb4"><span class="image"></span><span class="text">сочные Ягоды</span></a></li>
                            <li class="link-5"><a href="#bb5"><span class="image"></span><span class="text">Вкуснейшие сухофрукты</span></a></li>
                        </ul>
                    </nav>
                </div>
            </header>
            
            <div>
                {{$page->description}}
            </div>
            
        <footer>
            <div class="footer-inr1">
                <ul>
                    <li>
                        <a href="#">
                            <div class="image"><img src="images/footer-im1.png" alt="" /></div>
                            <div class="title">ЛЕГЕНДА</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="image"><img src="images/footer-im2.png" alt="" /></div>
                            <div class="title">Наша Команда</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="image"><img src="images/footer-im3.png" alt="" /></div>
                            <div class="title">Доставка</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="image"><img src="images/footer-im4.png" alt="" /></div>
                            <div class="title">КОРПОРАТИВНЫМ КЛИЕНТАМ</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="footer-inr2">
                <div class="copy">© Все права защищены ООО «Вита Мин»</div>
                <div class="dev"><a href="#"></a></div>
                <div class="soc">
                    <div>Давайте дружить!</div>
                    <a href="#" class="vk"></a>
                    <a href="#" class="fa"></a>
                    <a href="#" class="tw"></a>
                    <a href="#" class="go"></a>
                </div>
            </div>
        </footer>            
            
        
        
        <div class="fixes-s"></div>
        
        
        
    </body>
</html>
