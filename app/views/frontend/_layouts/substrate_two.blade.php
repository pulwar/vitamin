<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <title>ВИТАМИНЫ</title>
        <meta name="description" content="">
        <meta name="author" content="admin">

        <!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
        <link rel="shortcut icon" href="images/favicon.ico">
        <link rel="apple-touch-icon" href="images/favicon.png">
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/cusel.css" />
        
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/cusel-min-2.5.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function(){
                var params = {
                        changedEl: "select"
                    }
                cuSel(params);
            })
        </script>
        
        <script type="text/javascript">
            $(document).ready(function(){               
                
                
                $('.bxslider').bxSlider({
                  mode: 'fade',
                  infiniteLoop: false
                });
                $('.bxslider2').bxSlider({
                  mode: 'fade',
                  infiniteLoop: false
                });
            })
        </script>
        
        <!--[if lt IE 9]>
         <script type="text/javascript">
          var e = ("article,aside,figcaption,figure,footer,header,hgroup,nav,section,time").split(',');
          for (var i = 0; i < e.length; i++) {document.createElement(e[i]);}
         </script>
        <![endif]-->
    </head>

    <body class="body-tm">
        
        <div id="page">
            
            <header class="header-v2 header-v3">
                <div id="header">
                    <div class="header-top">
                        <div class="header-top-inr">
                            <a href="#" id="logo"><img src="images/logo-2.png" alt="" /></a>
                            <nav>
                                <ul>
                                    <li><a href="#">ФРУКТЫ</a></li>
                                    <li><a href="#">СЕТЫ</a></li>
                                    <li><a href="#">ЯГОДЫ</a></li>
                                    <li><a href="#">ОВОЩИ</a></li>
                                    <li><a href="#">СУХОФРУКТЫ</a></li>
                                </ul>
                            </nav>
                            <div class="phone">
                                <p>8<i></i>800<i></i>345<i></i>0000</p>
                                <div>Круглосуточная доставка свежижих фруктов и овощей по Москве</div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            
            <div id="content" class="bg-c">
                
                <div class="team-slider">
                    <div class="top-slider">
                        <div class="first-sl">
                            <ul class="bxslider">
                                <li style="background-image: url(images/slide-1.jpg);">
                                    <div class="text">В год от падающих кокосов погибает <br />в 15 раз больше людей, чем из-за нападения акул. Но про кокосы не снято ни одного фильма ужасов.</div>
                                </li>
                                <li style="background-image: url(images/slide-2.jpg);">
                                    <div class="text">В год от падающих кокосов погибает в 15 раз больше людей, чем из-за нападения акул. Но про кокосы не снято ни одного фильма ужасов.</div>
                                </li>
                                <li style="background-image: url(images/slide-1.jpg);">
                                    <div class="text">В год от падающих кокосов погибает <br />в 15 раз больше людей, чем из-за нападения акул. Но про кокосы не снято ни одного фильма ужасов.</div>
                                </li>
                                <li style="background-image: url(images/slide-2.jpg);">
                                    <div class="text">В год от падающих кокосов погибает в 15 раз больше людей, чем из-за нападения акул. Но про кокосы не снято ни одного фильма ужасов.</div>
                                </li>
                                <li style="background-image: url(images/slide-1.jpg);">
                                    <div class="text">В год от падающих кокосов погибает <br />в 15 раз больше людей, чем из-за нападения акул. Но про кокосы не снято ни одного фильма ужасов.</div>
                                </li>
                            </ul>
                        </div>
                        <div class="second-sl" style="display: none;">
                            <ul class="bxslider2">
                                
                                <li style="background-image: url(images/slide-2.jpg);">
                                    <div class="text">В год от падающих кокосов погибает в 15 раз больше людей, чем из-за нападения акул. Но про кокосы не снято ни одного фильма ужасов.</div>
                                </li>
                                <li style="background-image: url(images/slide-1.jpg);">
                                    <div class="text">В год от падающих кокосов погибает <br />в 15 раз больше людей, чем из-за нападения акул. Но про кокосы не снято ни одного фильма ужасов.</div>
                                </li>
                                <li style="background-image: url(images/slide-2.jpg);">
                                    <div class="text">В год от падающих кокосов погибает в 15 раз больше людей, чем из-за нападения акул. Но про кокосы не снято ни одного фильма ужасов.</div>
                                </li>
                                <li style="background-image: url(images/slide-1.jpg);">
                                    <div class="text">В год от падающих кокосов погибает <br />в 15 раз больше людей, чем из-за нападения акул. Но про кокосы не снято ни одного фильма ужасов.</div>
                                </li>
                                <li style="background-image: url(images/slide-2.jpg);">
                                    <div class="text">В год от падающих кокосов погибает в 15 раз больше людей, чем из-за нападения акул. Но про кокосы не снято ни одного фильма ужасов.</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                    <div id="bx-pager">
                        <a data-slide-index="0" href="" class="bx-p-1 active">                          
                            <div class="image">
                                <img src="images/p-ima-1.png" alt="" /> 
                            </div>
                            <div class="all">
                                {{$page->description}}
                            </div>
                        </a>
                        <a data-slide-index="1" href=""  class="bx-p-2">                            
                            <div class="image">
                                <img src="images/p-ima-2.png" alt="" /> 
                            </div>
                            <div class="all">
                                {{$page->description_two}}
                            </div>
                        </a>
                    </div>
                </div>
                
                <div class="container-product connect">
                    <div class="container-product-inr">
                        <div class="form-connect">
                            <h3>Присоедняйтесь к  нашей команде!</h3>
                            <form>
                                <input placeholder="Ваш номер телефона" type="text" />
                                <button><span>ПРИМИТЕ К СЕБЕ</span></button>
                            </form>                         
                        </div>
                    </div>
                </div>
                
            </div>

        </div>   
            
        <footer>
            <div class="footer-inr1">
                <ul>
                    <li>
                        <a href="#">
                            <div class="image"><img src="images/footer-im1.png" alt="" /></div>
                            <div class="title">ЛЕГЕНДА</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                    <li class="active">
                        <a href="#">
                            <div class="image"><img src="images/footer-im2a.png" alt="" /></div>
                            <div class="title">Наша Команда</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="image"><img src="images/footer-im3.png" alt="" /></div>
                            <div class="title">Доставка</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="image"><img src="images/footer-im4.png" alt="" /></div>
                            <div class="title">КОРПОРАТИВНЫМ КЛИЕНТАМ</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="footer-inr2">
                <div class="copy">© Все права защищены ООО «Вита Мин»</div>
                <div class="dev"><a href="#"></a></div>
                <div class="soc">
                    <div>Давайте дружить!</div>
                    <a href="#" class="vk"></a>
                    <a href="#" class="fa"></a>
                    <a href="#" class="tw"></a>
                    <a href="#" class="go"></a>
                </div>
            </div>
        </footer>
        
        <div class="fixes-s"></div>      
        
        
        <script type="text/javascript">
            $(document).ready(function(){
                $('#bx-pager .bx-p-1').click(function(){
                    $(this).addClass('active');
                    $('#bx-pager').removeClass('eq2');
                    $('#bx-pager .bx-p-2').removeClass('active');
                    $('.second-sl').css('display','none');
                    $('.first-sl').fadeIn();
                     return false;          
                })
                $('#bx-pager .bx-p-2').click(function(){
                    $('#bx-pager').addClass('eq2');
                    $(this).addClass('active');
                    $('#bx-pager .bx-p-1').removeClass('active');
                    $('.first-sl').css('display','none');
                    $('.second-sl').fadeIn();
                    return false;                       
                })
                $('#bx-pager .bx-p-2').after('<div class="spacer"></div>');
                $('#bx-pager .bx-p-2').after('<div class="spacer spacer2"></div>');
                var w_1 = $(window).width();
                var w_2 = $('#bx-pager').width();
                var w_3 = (w_1-w_2)/2;
                $('.spacer').each(function(){
                    $(this).css('width', w_3).css('left', -w_3);
                })
                $('.spacer2').css('right', -w_3).css('left','auto');
            })
            $(window).resize(function(){
                var w_1 = $(window).width();
                var w_2 = $('#bx-pager').width();
                var w_3 = (w_1-w_2)/2;
                $('.spacer').each(function(){
                    $(this).css('width', w_3).css('left', -w_3);
                })
                $('.spacer2').css('right', -w_3).css('left','auto');
            })
            
        </script>
        
    </body>
</html>
