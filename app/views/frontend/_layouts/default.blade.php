<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <title>ВИТАМИНЫ</title>
        <meta name="description" content="">
        <meta name="author" content="admin">

        <!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
        <link rel="shortcut icon" href="images/favicon.ico">
        <link rel="apple-touch-icon" href="images/favicon.png">
        {{HTML::style('css/style.css')}}       
        {{HTML::style('css/cusel.css')}}
        
        {{HTML::script('js/jquery.min.js')}}
        {{HTML::script('js/main.js')}}
        {{HTML::script('js/cusel-min-2.5.js')}}
       
        
        <script type="text/javascript">
            $(document).ready(function(){
                var params = {
                        changedEl: "select"
                    }
                cuSel(params);
            })
        </script>
        
        <!--[if lt IE 9]>
         <script type="text/javascript">
          var e = ("article,aside,figcaption,figure,footer,header,hgroup,nav,section,time").split(',');
          for (var i = 0; i < e.length; i++) {document.createElement(e[i]);}
         </script>
        <![endif]-->
    </head>

    <body>
        
        <div id="page">

            @yield('header')
            
            @yield('content') 
            
        </div>   
            
        <footer>
            <div class="footer-inr1">
                <ul>
                    <li>
                        <a href="#">
                            <div class="image">{{ HTML::image('images/footer-im1.png', $alt="", $attributes = array()) }}</div>
                            <div class="title">ЛЕГЕНДА</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="image">{{ HTML::image('images/footer-im2.png', $alt="", $attributes = array()) }}</div>
                            <div class="title">Наша Команда</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="image">{{ HTML::image('images/footer-im3.png', $alt="", $attributes = array()) }}</div>
                            <div class="title">Доставка</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="image">{{ HTML::image('images/footer-im4.png', $alt="", $attributes = array()) }}</div>
                            <div class="title">КОРПОРАТИВНЫМ КЛИЕНТАМ</div>
                            <div class="text">Мы любим своё дело, и с радостью делимся с Вами им.</div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="footer-inr2">
                <div class="copy">© Все права защищены ООО «Вита Мин»</div>
                <div class="dev"><a href="#"></a></div>
                <div class="soc">
                    <div>Давайте дружить!</div>
                    <a href="#" class="vk"></a>
                    <a href="#" class="fa"></a>
                    <a href="#" class="tw"></a>
                    <a href="#" class="go"></a>
                </div>
            </div>
        </footer>            
            
        <div class="popup-order">
            <div class="number"><span>5</span></div>
            <div class="details">В Вашем заказе <b id="cartProdCount">5</b> <span class="productsWord">продуктов</span> на сумму <b id="cartPrice">1 500</b> <i class="icon-rub2"></i><span class="delivery">, стоимость доставки <b>{{$delivery}}</b> <i class="icon-rub2"></i>, бесплатная доставка от <b>{{$freeDelivery}}</b> <i class="icon-rub2"></i></span></div>
            <a href="#" class="btn-bay btn-bay2"><span>Оформить заказ</span></a>
            <span class="label-gift"></span>
        </div>
        
        <div class="fixes-s"></div>
        
        @yield('products')
        
    </body>
</html>
