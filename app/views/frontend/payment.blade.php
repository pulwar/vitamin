@extends('frontend._layouts.default')
@section('header')
<header>
    <div id="header">
        <div class="header-inr1 clearfix">
            <a href="#" id="logo">{{ HTML::image('images/logo.png', $alt="", $attributes = array()) }}</a>
            <div class="phone">
                <p>8<i></i>800<i></i>345<i></i>0000</p>
                <div>Круглосуточная доставка свежижих фруктов и овощей по Москве</div>
            </div>
        </div>
        <div class="header-inr2">            
            {{ HTML::image('images/img-1.png', $alt="", $attributes = array()) }}
            <a href="#" class="btn-catalog">ПОСМОТРЕТЬ ПРИЛАВОК</a>
        </div>        
    </div>
</header>
@stop 
@section('content')
<div id="content">
    @if($success == 0)
        <h1>Заказ не оплачен</h1>
        <p>Попробуйте еще!</p>
    @elseif($success == 1)
        <h1>Заказ оплачен</h1>
    @endif
    @if($success == 0 || $success == 3)
	{{$form}} 
    @endif
    
</div>
@stop 