@extends('frontend._layouts.default')
@section('header')
<header>
    <div id="header">
        <div class="header-inr1 clearfix">
            <a href="#" id="logo"><img src="images/logo.png" alt="" /></a>
            <div class="phone">
                <p>8<i></i>800<i></i>345<i></i>0000</p>
                <div>Круглосуточная доставка свежижих фруктов и овощей по Москве</div>
            </div>
        </div>
        <div class="header-inr2">
            <img src="images/img-1.png" alt="" />
            <a href="#" class="btn-catalog">ПОСМОТРЕТЬ ПРИЛАВОК</a>
        </div>
        <nav id="top-nav">
            <div class="image-top"></div>
            <ul>
            	@foreach($categories as $category)
            	<li class="link-{{$category->img}}"><a href="#bb{{$category->id}}"><span class="image"></span><span class="text">{{$category->name}}</span></a></li>
            	@endforeach                
            </ul>
        </nav>
    </div>
</header>
@stop 
@section('content')
<div id="content" data-free-delivery="{{$freeDelivery}}" data-delivery="{{$delivery}}">
	@foreach($categories as $category)
    <div class="container-product container-product-first clearfix">
        <div class="container-product-inr">
            <div class="title" id="bb{{$category->id}}"><span><span>{{$category->name}}</span></span></div>
            <div class="title2">{{$category->description}}</div>
        
            <div class="product-list">
                {{--*/ $counter = 0/*--}}
            	@foreach($category->products as $product)
                    @if(!$product->hide)
	                {{--*/ $counter = $counter + 1/*--}}
                    <div class="product-item" @if($counter>$category->products_count) style="display:none;" @endif data-product-id="{{$product->id}}">
	                    <div class="image"><a href="#"><img src="{{ $product->img->url('thumb') }}" ></a></div>
	                    <div class="name-desk">
	                        <a href="#">{{$product->name}}</a>
	                        <p>{{$product->description}}</p>
	                    </div>
	                    <div class="price-bay">
	                        <div class="price-num">
                                @if($productsInCart and array_key_exists($product->id, $productsInCart))
                                    <div class="number"><i class="minus"></i><span><span class="prodCount" data-step="{{$product->step}}">{{$productsInCart[$product->id]}}</span> {{$product->unit}}</span><i class="plus"></i></div>
                                    <input type="hidden" value="{{$productsInCart[$product->id]}}" class="inp-val" />
                                    
                                    @if($product->oldPrice)   
                                        <div class="pirce pirce2">                                     
                                            <span class="old"><span>{{($product->oldPrice)*($productsInCart[$product->id])}}</span><i class="icon-rub"></i></span>
                                            <span class="new"><span class="price">{{($product->price)*($productsInCart[$product->id])/$product->step}}</span><i class="icon-rub"></i></span>                                        
                                        </div>
                                    @else
                                        <div class="pirce">    
                                            <span class="price">{{($product->price)*($productsInCart[$product->id])/$product->step}}</span><i class="icon-rub"></i>                                     
                                        </div>
                                    @endif                                   
                                @else
                                    <div class="number"><i class="minus"></i><span><span class="prodCount" data-step="{{$product->step}}">{{$product->step}}</span> {{$product->unit}}</span><i class="plus"></i></div>
                                    <input type="hidden" value="{{$product->step}}" class="inp-val" />
                                    
                                    @if($product->oldPrice) 
                                        <div class="pirce pirce2">
                                            <span class="old"><span>{{$product->oldPrice}}</span><i class="icon-rub"></i></span>
                                            <span class="new"><span class="price">{{$product->price}}</span><i class="icon-rub"></i></span>
                                        </div>                                  
                                    @else
                                        <div class="pirce">
                                            <span class="price">{{$product->price}}</span><i class="icon-rub"></i>
                                        </div>
                                    @endif 
                                    
                                @endif
                                    <input type="hidden" value="{{$product->price}}" class="inp-price-new" />
                                    <input type="hidden" value="{{$product->oldPrice}}" class="inp-price" />          	                           
	                        </div>
                            @if($productsInCart and array_key_exists($product->id, $productsInCart))
                                <a href="#" class="btn-bay btn-bay2" data-product-id="{{$product->id}}"><span>ОФОРМИТЬ</span></a>
                            @else
                                <a href="#" class="btn-bay" data-product-id="{{$product->id}}"><span>ЗАКАЗАТЬ</span></a>
                            @endif	                        
	                    </div>                      
	                </div>             
                    @endif 
                @endforeach
                @if($counter>$category->products_count)
                    <button class="showMore">Больше</button>
                @endif
            </div>  
        </div>
    </div>
    @endforeach     
    
</div>
@stop
@section('products')
<div class="popup-container">
                
            <div class="popup-bg"></div>
            <div class="popup order-big">
                <div class="title"><span><span>ВАШ ЗАКАЗ</span></span></div>
                <div class="message deliveryMessage">Закажите продуктов на <b>{{$freeDelivery}} Р</b> и мы <br /><b>доставим Ваш заказ бесплатно!</b></div>
                <div class="items-table">
                    <table>
                        <thead>
                            <tr>
                                <td class="td-1"></td>
                                <td class="td-2"></td>
                                <td class="td-3">Помыть</td>
                                <!--td class="td-4">Почистить</td-->
                                <td class="td-5">Кол-во</td>
                                <td class="td-6">Цена</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                                @if($productsInCart and array_key_exists($product->id, $productsInCart))
                                <tr data-product-id="{{$product->id}}">
                                @else
                                <tr data-product-id="{{$product->id}}" style="display:none;">
                                @endif
                                    <td><a href="#"><img src="{{ $product->img->url('thumb') }}" ></a></td>
                                    <td>
                                        <a href="#" class="name">{{$product->name}}</a>
                                        <p><b>{{$product->price}} р. {{$product->step}} {{$product->unit}},</b> {{$product->packing}}  ±  {{$product->step}} {{$product->unit}}</p>
                                    </td>
                                    <td><i class="icon-wash" data-wash='Помыть {{$product->shortName}}'></i></td>
                                    <!--td><i class="icon-clean"></i></td-->
                                    <td>
                                        <div class="num">
                                            <div class="number"><i class="minus"></i><span><span class="prodCount" data-step="{{$product->step}}">{{$product->step}}</span> {{$product->unit}}</span><i class="plus"></i></div>
                                            <input type="hidden" value="1" class="inp-val">
                                            <a href="#" class="remove">Удалить из заказа</a>
                                        </div>
                                    </td>
                                    <td><span class="price" data-product-price="{{$product->price}}" data-step="{{$product->step}}">{{$product->price}}</span><i class="icon-rub"></i></td>
                                </tr>                                   
                            @endforeach                       
                        </tbody>
                    </table>
                </div>
                <div class="total">
                    <div class="total-inr">
                        <div class="summ">    
                        @foreach($products as $product)                                                   
                                <div data-prod-id="{{$product->id}}" style="display:none;"><span>Помыть {{$product->shortName}}</span><span>0<i class="icon-rub"></i></span></div>                            
                        @endforeach                                                    
                            <div><span>Сумма заказа</span><span class="orderPrice">1 325<i class="icon-rub"></i></span></div>
                            <div class="delivery" style="display:none;"><span>Доставка</span><span>{{$delivery}}<i class="icon-rub"></i></span></div>
                            <div class="disc" style="display:none;"><span>Скидка</span><span><span class="disc"></span><i class="icon-rub3"></i></span></div>
                            <div class="end"><span>Итого</span><span><span class="s-total">1 325</span><i class="icon-rub3"></i></span></div>
                            <a href="#" class="btn-bay btn-bay2"><span>ОФОРМИТЬ ЗАКАЗ</span></a>
                        </div>
                    </div>
                </div>
            </div>
        
        
            <div class="popup order-end">
                <div class="title color-2"><span><span>Оформление заказа</span></span></div>
                <div class="order-data">
                    <form class="orderData">
                        <div class="line">
                            <label>Ваше имя</label>
                            <div class="field"><input type="text" class="" name="cart[name]" value="" id="name" /></div>
                        </div>
                        <div class="line">
                            <label>Номер телефона</label>
                            <div class="field"><input type="text" value="" class="" name="cart[phone]" id="phone-number" /></div>
                        </div>
                        <div class="line">
                            <label>Адрес доставки</label>
                            <div class="field">
                                <div class="label">
                                    <span class="lab-1">Город</span>
                                    <span class="lab-2">Улица</span>
                                    <span class="lab-3">Дом</span>
                                </div>
                                <select name="cart[city]">
                                    <option value="1" selected="selected">Москва</option>
                                    <option value="2">Реутов</option>
                                    <option value="3">Балашиха</option>
                                    <option value="3">Восточный</option>
                                    <option value="3">Долгопрудный</option>
                                </select>
                                <input type="text" value="Первомайская" class="valid street" name="cart[street]"/>
                                <input type="text" value="2" class="valid house" name="cart[house]"/>
                            </div>
                        </div>
                        <div class="line">
                            <label>Промокод</label>
                            <div class="field"><input type="text" value="" class="promo" /></div>
                        </div>
                    </form>
                </div>
                <div class="total">
                    <div class="total-inr">
                        <div class="summ">   
                        @foreach($products as $product)                                                   
                            <div data-prod-id="{{$product->id}}" style="display:none;"><span>Помыть {{$product->shortName}}</span><span>0<i class="icon-rub"></i></span></div>                        
                        @endforeach
                            <div><span>Сумма заказа</span><span class="orderPrice">1 325<i class="icon-rub"></i></span></div>                            
                            <div class="delivery" style="display:none;"><span>Доставка</span><span>{{$delivery}}<i class="icon-rub"></i></span></div>
                            <div class="disc" style="display:none;"><span>Скидка</span><span><span class="disc"></span><i class="icon-rub3"></i></span></div>
                            <div class="end"><span>Итого</span><span><span class="s-total">1 325</span><i class="icon-rub3"></i></span></div>
                            <a href="#" class="btn-bay btn-bay2 disabled"><span>ОФОРМИТЬ ЗАКАЗ</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="popup order-ok">
                <div class="empty">
                    <i class="icon-ok"></i>
                    <p>Ваш заказ оформлен, в ближайшее время наш менеджер свяжется с Вами.</p>
                    <a href="#" class="btn-back"><span>ОК</span></a>
                </div>
            </div>
        </div>
@stop