<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>My Awesome Blog</title>

    {{ HTML::style('css/admin.css') }}

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    @yield('head')
</head>
<body>

<header>
    <div class="container">
        <h1>Admin Panel</h1>
        <p>By PuLWaR</p>
    </div>
    <div style="position: relative;float: right;padding: 0px 2%;">
        {{link_to_action('Jacopo\Authentication\Controllers\AuthController@getLogout','Выход')}}
    </div> 
</header>

<main class="container">
    @yield('content')
</main>

<footer>
    <div class="container">
        &copy; {{ date('Y') }} My Admin Panel 
    </div>
</footer>
</body>
</html>