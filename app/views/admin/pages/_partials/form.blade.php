<ul>	
	<li>
		{{ Form::label('name','Название') }}
		{{ Form::text('name') }}
		{{ $errors->first('name','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('url','URL') }}
		{{ Form::text('url') }}
		{{ $errors->first('url','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('substrate','Подложка') }}
		{{ Form::select('substrate',array('1' => 'Одно описание','2' => 'Два описаня','5' => 'Пять описаний')) }}
		{{ $errors->first('substrate','<p class="error">:message</p>') }}	
	</li>
	<li>
		{{ Form::label('description','Описание1') }}
		{{ Form::textarea('description',null,array('class' => 'ckeditor')) }}
		{{ $errors->first('description','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('description_two','Описание2') }}
		{{ Form::textarea('description_two',null,array('class' => 'ckeditor')) }}
		{{ $errors->first('description_two','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('description_three','Описание3') }}
		{{ Form::textarea('description_three',null,array('class' => 'ckeditor')) }}
		{{ $errors->first('description_three','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('description_four','Описание4') }}
		{{ Form::textarea('description_four',null,array('class' => 'ckeditor')) }}
		{{ $errors->first('description_four','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('description_five','Описание5') }}
		{{ Form::textarea('description_five',null,array('class' => 'ckeditor')) }}
		{{ $errors->first('description_five','<p class="error">:message</p>') }}		
	</li>		
	<li>		
		{{ Form::submit('Сохранить') }}		
		
	</li>
</ul>