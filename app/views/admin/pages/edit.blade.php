@extends('admin._layouts.admin')
@section('content')
{{ link_to_route('admin.pages.index','Назад')}}
	{{ Form::model($page, array('route' => array('admin.pages.update',$page->id),'method' => 'put'))}}
		@include('admin.pages._partials.form')
	{{ Form::close()}}
	{{HTML::script('js/ckeditor/ckeditor.js')}}	
@stop 