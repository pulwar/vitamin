@extends('admin._layouts.admin')

@section('content')
<h1>Страницы</h1>
@if(count($pages))
	<ul>
	@foreach($pages as $page)	
		<li>	
			{{ link_to_route('admin.pages.edit', $page->name, array($page->id))}}
		</li>
	@endforeach
	</ul>
@endif
@stop 