<ul>
	<li>
		{{ Form::label('hide','Скрыть на главной странице?') }}
		{{ Form::select('hide',array('0' => 'Нет','1' => 'Да')) }}
		{{ $errors->first('hide','<p class="error">:message</p>') }}	
	</li>
	<li>
		{{ Form::label('name','Категория') }}
		{{ Form::select('category_id',Category::lists('name','id') ) }}
		{{ $errors->first('category_id','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::label('status_id','Статус') }}
		{{ Form::select('status_id',array(''=>'Не выбран','Статусы'=>Status::lists('name','id')) ) }}
		{{ $errors->first('status_id','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::label('price','Стоимость') }}
		{{ Form::text('price') }}
		{{ $errors->first('price','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::label('oldPrice','Стоимость до скидки') }}
		{{ Form::text('oldPrice') }}
		{{ $errors->first('oldPrice','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::label('unit','Шаг измерения (например, “кг.”, “г.”, “шт.”)') }}
		{{ Form::text('unit') }}
		{{ $errors->first('unit','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::label('step','Значение шага (число, например, 1 или 1000)') }}
		{{ Form::text('step') }}
		{{ $errors->first('step','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::label('wash_flag','Можно мыть?') }}
		{{ Form::select('wash_flag',array('0' => 'Нет','1' => 'Да')) }}
		{{ $errors->first('wash_flag','<p class="error">:message</p>') }}	
	</li>
	<li>
		{{ Form::label('name','Наименование') }}
		{{ Form::text('name') }}
		{{ $errors->first('name','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::label('packing','Способ поставки') }}
		{{ Form::text('packing') }}
		{{ $errors->first('packing','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::label('shortName','Короткое наименование в чеке') }}
		{{ Form::text('shortName') }}
		{{ $errors->first('shortName','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::label('description','Описание') }}
		{{ Form::textarea('description') }}
		{{ $errors->first('description','<p class="error">:message</p>') }}
	</li>	
	<li>
		{{ Form::file('img')}}
	</li>
	<li>
		{{ Form::submit('Сохранить') }}		
	</li>
</ul>