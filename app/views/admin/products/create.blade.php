@extends('admin._layouts.admin')

@section('content')
	{{ link_to_route('admin.categories.index', 'Назад')}}
	<h1>Новый продукт</h1>	
	{{ Form::open(array('route' => 'admin.products.store','method' => 'post','files' => true))}}
		@include('admin.products._partials.form')
	{{ Form::close()}}	
@stop 