@extends('admin._layouts.admin')

@section('content')
	
	{{ link_to_route('admin.categories.index', 'Назад')}}

	<h1>Редактирование продукта</h1>

	<img src="{{ $product->img->url('thumb') }}" >
	{{ Form::model($product, array('route' => array('admin.products.update',$product->id),'method' => 'post','files' => true))}}
		@include('admin.products._partials.form')
	{{ Form::close()}}	
@stop 