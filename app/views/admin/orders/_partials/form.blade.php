<ul>
	<li>
		{{ Form::label('orderStatus_id','Статус') }}
		{{ Form::select('orderStatus_id',OrderStatus::lists('name','id')) }}
		{{ $errors->first('orderStatus_id','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('name','ФИО') }}
		{{ Form::text('name') }}
		{{ $errors->first('name','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('address','Адрес') }}
		{{ Form::text('address') }}
		{{ $errors->first('address','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('phone','Телефон') }}
		{{ Form::text('phone') }}
		{{ $errors->first('phone','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('price','Стоимость') }}
		{{ Form::text('price') }}
		{{ $errors->first('price','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('paid','Оплата') }}
		{{ Form::text('paid') }}
		{{ $errors->first('paid','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('description','Комментарий') }}
		{{ Form::text('description') }}
		{{ $errors->first('description','<p class="error">:message</p>') }}		
	</li>
	<li>		
		{{ Form::submit('Сохранить') }}		
		
	</li>
</ul>