@extends('admin._layouts.admin')

@section('content')
	{{ link_to_route('admin.orders.index','Назад')}}
	{{ Form::open(array('route' => array('admin.orders.store')))}}
		@include('admin.orders._partials.form')
	{{ Form::close()}}
@stop