@extends('admin._layouts.admin')

@section('content')
{{ link_to_route('admin.orders.create','Добавить заказ')}}
{{ Form::open(array('route' => array('admin.orders.index'),'method' => 'get'))}}
	<input name="find" @if($find) value="{{$find}}" @endif>
	{{ Form::submit('Найти') }}			
{{ Form::close()}}
{{ Form::open(array('route' => array('admin.orders.index'),'method' => 'get'))}}
	C: <input name="fromDate" type="date" value="{{$fromDate}}">По: <input name="underDate" type="date" value="{{$underDate}}"> 
		<select name="status">
			<option value="0">Все</option>
			@foreach(OrderStatus::lists('name','id') as $key => $val)
				<option @if($status == $key)selected @endif value="{{$key}}">{{$val}}</option>
			@endforeach
		</select>
	{{ Form::submit('Найти') }}			
{{ Form::close()}}
<h1>Заказы</h1>

@if(count($orders))
	<ul>
	@foreach($orders as $order)
		<li>			
			{{ link_to_route('admin.orders.show', $order->name, array($order->id))}}
			{{ Form::open(array('route' => array('admin.orders.destroy',$order->id),'method' => 'delete', 'class' => 'destroy'))}}
			{{ Form::submit('Удалить')}}
			{{ Form::close()}}			
		</li>
	@endforeach
	</ul>
@else
<p>Нет заказов</p>
@endif

@stop 