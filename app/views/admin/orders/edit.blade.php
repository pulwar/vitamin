@extends('admin._layouts.admin')
@section('content')
{{ link_to_route('admin.orders.show','Назад',array($order->id))}}
	{{ Form::model($order, array('route' => array('admin.orders.update',$order->id),'method' => 'put'))}}
		@include('admin.orders._partials.form')
	{{ Form::close()}}
@stop 