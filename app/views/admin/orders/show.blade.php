@extends('admin._layouts.admin')

@section('content')
{{ link_to_route('admin.orders.index','Назад') }}
<h1>{{link_to_route('admin.orders.edit','Редактировать заказ',array($order->id))}}</h1>
<p>Статус: {{$order->status->name}}</p>
@if(count($order->client_id))
	<p>{{link_to_route('admin.clients.edit', $order->name, array($order->client_id))}}</p>
@else
	<p>ФИО: {{$order->name}}</p>
@endif
<p>Тел: {{$order->phone}}</p>
<p>адрес: {{$order->address}}</p>
<p>Стоимость: {{$order->price}}</p>
<ul>
@foreach($productsOfOrder as $product)
	<li>
		{{ link_to_route('admin.pof.edit',$product->product->name, array($product->id))}} {{{$product->count}}} кг 
		{{ Form::open(array('route' => array('admin.pof.destroy',$product->id), 'method' => 'delete', 'class' => 'destroy'))}}
		{{ Form::submit('Удалить')}}
		{{ Form::close()}}
	</li>
@endforeach
</ul>
{{ link_to_route('admin.pof.create','Добавить продукт',array($order->id))}}
@stop 