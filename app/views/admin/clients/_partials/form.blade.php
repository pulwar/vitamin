<ul>
	<li>
		{{ Form::label('name','ФИО') }}
		{{ Form::text('name') }}
		{{ $errors->first('name','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::label('address','Адрес') }}
		{{ Form::text('address') }}
		{{ $errors->first('address','<p class="error">:message</p>') }}
	</li>	
	<li>
		{{ Form::label('phone','Телефон') }}
		{{ Form::text('phone') }}
		{{ $errors->first('phone','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::label('email','Email') }}
		{{ Form::text('email') }}
		{{ $errors->first('email','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::submit('Сохранить') }}		
	</li>
</ul>