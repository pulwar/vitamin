@extends('admin._layouts.admin')

@section('content')
{{ link_to_route('admin.orders.create','Добавить клмента (создать заказ)')}}
<h1>Клиенты</h1>
{{ Form::open(array('route' => array('admin.clients.index'),'method' => 'get'))}}
	<input name="find">
	{{ Form::submit('Найти') }}			
{{ Form::close()}}
@if(count($clients))
<table class="table table-order table-hover">
    <colgroup>   
                            
        <col width="25%">   
        <col width="15%">   
        <col width="15%">
        <col width="15%">  
        <col width="30%">
        <col width="30%">
    </colgroup>
    <thead>    
	<tr>   		
     	<td>
 		@if($sortingBy =='clients.name')
 			@if($sortingAs=='asc')
 				{{link_to_route('admin.clients.index','ФИО &#9650;',array('sortingBy'=>'clients.name', 'sortingAs'=>'desc'))}}
 			@else
 				{{link_to_route('admin.clients.index','ФИО &#9660;',array('sortingBy'=>'clients.name', 'sortingAs'=>'asc'))}}
 			@endif
 		@else
 			{{link_to_route('admin.clients.index','ФИО',array('sortingBy'=>'clients.name', 'sortingAs'=>'asc'))}}
 		@endif
     	</td>
        <td>
    	@if($sortingBy =='clients.created_at')
 			@if($sortingAs=='asc')
 				{{link_to_route('admin.clients.index','Создан &#9650;',array('sortingBy'=>'clients.created_at', 'sortingAs'=>'desc'))}}
 			@else
 				{{link_to_route('admin.clients.index','Создан &#9660;',array('sortingBy'=>'clients.created_at', 'sortingAs'=>'asc'))}}
 			@endif
 		@else
 			{{link_to_route('admin.clients.index','Создан',array('sortingBy'=>'clients.created_at', 'sortingAs'=>'asc'))}}
 		@endif
        </td>
       	<td>
       	@if($sortingBy =='COUNT(`orders`.`id`)')
 			@if($sortingAs=='asc')
 				{{link_to_route('admin.clients.index','Заказы &#9650;',array('sortingBy'=>'COUNT(`orders`.`id`)', 'sortingAs'=>'desc'))}}
 			@else
 				{{link_to_route('admin.clients.index','Заказы &#9660;',array('sortingBy'=>'COUNT(`orders`.`id`)', 'sortingAs'=>'asc'))}}
 			@endif
 		@else
 			{{link_to_route('admin.clients.index','Заказы',array('sortingBy'=>'COUNT(`orders`.`id`)', 'sortingAs'=>'asc'))}}
 		@endif
       	</td>
        <td>
        @if($sortingBy =='SUM(`orders`.`price`)')
 			@if($sortingAs=='asc')
 				{{link_to_route('admin.clients.index','Сумма &#9650;',array('sortingBy'=>'SUM(`orders`.`price`)', 'sortingAs'=>'desc'))}}
 			@else
 				{{link_to_route('admin.clients.index','Сумма &#9660;',array('sortingBy'=>'SUM(`orders`.`price`)', 'sortingAs'=>'asc'))}}
 			@endif
 		@else
 			{{link_to_route('admin.clients.index','Сумма',array('sortingBy'=>'SUM(`orders`.`price`)', 'sortingAs'=>'asc'))}}
 		@endif	
        </td> 
        <td>
        </td>
        <td>
        </td>
	</tr>
	</thead>
	<tbody>
	@foreach($clients as $client)	
		<tr>			
			<td>{{ link_to_route('admin.clients.edit', $client->name, array($client->id))}}</td>
			<td>{{date('Y-m-d', strtotime($client->created_at))}}</td>
			<td>{{count($client->orders)}}</td>
			<td>{{$client['SUM(`orders`.`price`)']}}</td>		
			<td>{{$client->phone}}</td>
			<td>{{$client->email}}</td>			
		</tr>
	@endforeach
	</tbody>
</table>
@else
<p>Список пуст</p>
@endif
@stop 