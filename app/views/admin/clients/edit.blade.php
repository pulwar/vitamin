@extends('admin._layouts.admin')
@section('content')
{{ link_to_route('admin.clients.index','Назад')}}
	{{ Form::model($client, array('route' => array('admin.clients.update',$client->id),'method' => 'put'))}}
		@include('admin.clients._partials.form')
	{{ Form::close()}}
	@if($client->orders)
		<h1>Список заказов</h1>
		<ul>
		@foreach($client->orders as $order)
			<li>
				{{link_to_route('admin.orders.show','№ '.$order->id,array($order->id))}}
			</li>
		@endforeach
		</ul>
	@endif
@stop 