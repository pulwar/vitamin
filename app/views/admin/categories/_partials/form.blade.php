<ul>
	<li>
		{{ Form::label('name','Название') }}
		{{ Form::text('name') }}
		{{ $errors->first('name','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::label('description','Описание') }}
		{{ Form::textarea('description') }}
		{{ $errors->first('description','<p class="error">:message</p>') }}
	</li>	
	<li>
		{{ Form::label('products_count','Количество продуктов на главной') }}
		{{ Form::text('products_count') }}
		{{ $errors->first('products_count','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::submit('Сохранить') }}		
	</li>
</ul>