@extends('admin._layouts.admin')
@section('head')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>  
  <style>
	  #sortable1, #sortable2, #sortable3, #sortable4, #sortable5 { list-style-type: none; margin: 0; padding: 0; zoom: 1; }
	  #sortable1 li, #sortable2 li,#sortable3 li,#sortable4 li,#sortable5 li { margin: 0 5px 5px 5px; padding: 3px; width: 90%; }
  </style>
  <script>
  $(function() {  

    $( "#sortable1, #sortable2, #sortable3, #sortable4, #sortable5" ).sortable({
  		cancel: ".ui-state-disabled",
  		update: function(event, ui) { 
  			$.ajax({
                data:{arr:$(this).sortable('toArray')} ,
                type: 'POST',
                url: '/admin/products/order-update'
            })
      	}
    });
  }); 
  </script>
@stop
@section('content')
<h1>Категории и продукты</h1>

{{ link_to_route('admin.products.create', 'Добавить товар')}}

@if(count($categories))
	
	@foreach($categories as $category)
		
		 <h2>{{ link_to_route('admin.categories.edit', $category->name, array($category->id))}}</h2>
			<ul id="sortable{{$category->id}}">
			@foreach($category->products as $product)
				@if($product->hide)
					<li class="ui-state-default ui-state-disabled" id="{{$product->id}}">
				@else
					<li class="ui-state-default" id="{{$product->id}}" data-product-id="{{$product->id}}">
				@endif
						{{ link_to_route('admin.products.edit', $product->name, array($product->id))}}
						{{ Form::open(array('route' => array('admin.products.destroy',$product->id),'method' => 'delete', 'class' => 'destroy'))}}
						{{ Form::submit('Удалить')}}
						{{ Form::close()}}
					
				</li>			
			@endforeach	
			</ul>	
	@endforeach
	
@endif

@stop 