@extends('admin._layouts.admin')

@section('content')
	{{ link_to_route('admin.categories.index', 'Назад')}}
	<h1>Редактирование категории</h1>
	{{ Form::model($category, array('route' => array('admin.categories.update',$category->id),'method' => 'put'))}}
		@include('admin.categories._partials.form')
	{{ Form::close()}}
@stop 