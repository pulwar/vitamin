@extends('admin._layouts.admin')

@section('content')
	{{ link_to_route('admin.promos.index','Назад')}}
	{{ Form::open(array('route' => array('admin.promos.store')))}}
		@include('admin.promos._partials.form')
	{{ Form::close()}}
@stop