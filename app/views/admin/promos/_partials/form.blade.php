<ul>	
	<li>
		{{ Form::label('code','Код') }}
		{{ Form::text('code') }}
		{{ $errors->first('code','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('value','Величина') }}
		{{ Form::text('value') }}
		{{ $errors->first('value','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('notWork','Использован?') }}
		{{ Form::select('notWork',array('0'=>'Нет','1'=>'Да') ) }}
		{{ $errors->first('notWork','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::label('flagSingleUser','Один клиент?') }}
		{{ Form::select('flagSingleUser',array('1'=>'Да','0'=>'Нет') ) }}
		{{ $errors->first('flagSingleUser','<p class="error">:message</p>') }}
	</li>	
	<li>
		{{ Form::label('valueType','Тип скидки') }}
		{{ Form::select('valueType',array('%'=>'Процент','rur'=>'Сумма') ) }}
		{{ $errors->first('valueType','<p class="error">:message</p>') }}
	</li>
	<li>
		{{ Form::label('dateStart','Начало действия') }}
		{{ Form::input('date','dateStart') }}
		{{ $errors->first('dateStart','<p class="error">:message</p>') }}		
	</li>
	<li>
		{{ Form::label('dateEnd','Конец дествия') }}
		{{ Form::input('date','dateEnd') }}
		{{ $errors->first('dateEnd','<p class="error">:message</p>') }}		
	</li>
	<li>		
		{{ Form::submit('Сохранить') }}		
		
	</li>
</ul>