@extends('admin._layouts.admin')

@section('content')
{{ link_to_route('admin.promos.create','Добавить Код')}}
@if(count($promos))
	@foreach($promos as $promo)
		<li>			
			{{ link_to_route('admin.promos.edit', $promo->code, array($promo->id))}}
			{{ Form::open(array('route' => array('admin.promos.destroy',$promo->id),'method' => 'delete', 'class' => 'destroy'))}}
			{{ Form::submit('Удалить')}}
			{{ Form::close()}}			
		</li>
	@endforeach
	</ul>
@endif
@stop 