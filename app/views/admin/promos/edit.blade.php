@extends('admin._layouts.admin')

@section('content')
{{ link_to_route('admin.promos.index','Назад')}}
	{{ Form::model($promo, array('route' => array('admin.promos.update',$promo->id),'method' => 'put'))}}
		@include('admin.promos._partials.form')
	{{ Form::close()}}
@stop 