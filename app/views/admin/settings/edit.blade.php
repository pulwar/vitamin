@extends('admin._layouts.admin')
@section('content')
{{ link_to_route('admin.settings.index','Назад')}}
	<h1>{{$setting->name}}</h1>
	{{ Form::model($setting, array('route' => array('admin.settings.update',$setting->id),'method' => 'put'))}}
		@include('admin.settings._partials.form')
	{{ Form::close()}}	
@stop 