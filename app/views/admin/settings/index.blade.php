@extends('admin._layouts.admin')

@section('content')
<h1>Настройки</h1>
@if(count($settings))
	<ul>
	@foreach($settings as $setting)	
		<li>	
			{{ link_to_route('admin.settings.edit', $setting->name, array($setting->id))}}
		</li>
	@endforeach
	</ul>
@endif
@stop 