@extends('admin._layouts.admin')
@section('content')
{{ link_to_route('admin.orders.show','Назад',array($productsoforder->order_id))}}
	{{ Form::model($productsoforder, array('route' => array('admin.pof.update',$productsoforder->id),'method' => 'put'))}}
		@include('admin.productsOfOrders._partials.form')
	{{ Form::close()}}
@stop 