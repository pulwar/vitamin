@extends('admin._layouts.admin')

@section('content')
{{ link_to_route('admin.orders.show','Назад',array($oId))}}
	{{ Form::open(array('route' => array('admin.pof.store')))}}
		@include('admin.productsOfOrders._partials.form')		
		{{ Form::hidden('order_id',$oId)}}
	{{ Form::close()}}
@stop 