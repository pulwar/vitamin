<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 20) as $index)
		{
			Product::create([
				'name' => $faker->sentence(),
				'shortName' => $faker->sentence(), 
				'description' => $faker->realText(300),
				'order' => rand(1,5),
				'oldPrice' => rand(1,50),
				'price' => rand(40,80),
				'category_id' => rand(1,5)
			]);
		}
	}

}