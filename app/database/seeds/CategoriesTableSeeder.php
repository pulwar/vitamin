<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CategoriesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 5) as $index)
		{
			Category::create([
				'name' => $faker->sentence(), 
				'description' => $faker->realText(300),
				'order' => rand(1,5)
			]);
		}
	}

}