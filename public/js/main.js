$(document).ready(function(){
	$('.number i').click(function(){
		var val_i = parseInt($(this).parent().next().val());
		if($(this).attr('class') == 'plus') {				
			var step = parseInt($(this).prev().find('span').attr('data-step'));	
			val_i += step;
			$(this).parent().next().val(val_i); 
			$(this).prev().find('span').text(val_i);
		}
		if($(this).attr('class') == 'minus' && (val_i > $(this).next().find('span').attr('data-step'))) {			
			var step = parseInt($(this).next().find('span').attr('data-step'));
			val_i -= step;
			$(this).parent().next().val(val_i); 
			$(this).next().find('span').text(val_i);
		}
		var inp_p = $(this).parent().parent().find('.inp-price').val();
		var inp_p_n = $(this).parent().parent().find('.inp-price-new').val();
		var txt_p = $(this).parent().parent().find('.pirce span');
		var cart_p = $(this).parents('tr').find('span.price').attr('data-product-price');
		var prod_d = $(this).parents('div.product-item').attr('data-product-id');			
		if(!txt_p.attr('class')) {			
			txt_p.text(abc(inp_p*val_i));
		}
		if(val_i >= step){
			if(txt_p.attr('class') == 'old' ) {				
				txt_p.parent().find('.old span').text(abc(inp_p*val_i/step));
				txt_p.parent().find('.new span').text(abc(inp_p_n*val_i/step));
			} else{							
				txt_p.text(abc(inp_p_n*val_i/step));
			}	
		}
		
		if (cart_p){			
			//$(this).parents('tr').find('span.price').text(cart_p*val_i);
			var wash = 0;
			if($(this).parents('tr').find('i.icon-wash').hasClass('active')){
				wash = 1;
			}else{
				wash = 0;
			}
			var productId = $(this).parents('tr').attr('data-product-id');
			var count = $(this).parents('tr').find('span.prodCount').html();
			var price = ($(this).parents('tr').find('span.price').attr('data-product-price'))*($(this).parents('tr').find('span.prodCount').html()/step);
			
			$.ajax({				
	            url: './add-product',
	            type:'POST',
	            data:{
	            	productId:productId,
	            	count:count,
	            	price:price,
	            	wash:wash,
	            	},
	            dataType : 'json',
	            success: function(data){
	            	cartUpdate(data['cart']);	
	            	labelUpdate(data);
	            	//изменить на главной
	            	$('div.product-item[data-product-id="'+productId+'"] span.prodCount').html(count);
	            	$('div.product-item[data-product-id="'+productId+'"] div.pirce').html('<span>'+price+'</span><i class="icon-rub"></i>');
	            }                
	        });
		}
		if(prod_d){			
			if($('a[data-product-id="'+prod_d+'"]').hasClass('btn-bay2')){				
				$.ajax({				
		            url: './add-product',
		            type:'POST',
		            data:{
		            	productId:prod_d,
		            	count:val_i,
		            	price:($('div[data-product-id="'+prod_d+'"] div.pirce span.price').html()).replace(' ',''),		            	
		            	},
		            dataType : 'json',
		            success: function(data){
		            	cartUpdate(data['cart']);
		            	labelUpdate(data);
		            }                
		        });
			}			
		}
	})
	
	
	$(".btn-catalog").click(function() {
    $('html, body').animate({
	        scrollTop: $(".container-product-first").offset().top+60
	    }, 1000);
	    return false;
	});
	
	
	$('.price-bay .btn-bay').click(function(){		
		if($(this).attr('class') == 'btn-bay') {			
			$('.popup-order').fadeIn();
			$('.popup-order').animate({ bottom: '-1px'   }, 400);
		}
		return false;
	})
	
	function abc(n) {
	    return (n + "").split("").reverse().join("").replace(/(\d{3})/g, "$1 ").split("").reverse().join("").replace(/^ /, "");
	}

	$('.icon-wash').click(function(){
		$(this).toggleClass('active');
		var wash = 0;
		if($(this).attr('class') != 'icon-wash' ) {
			wash = 1;
		}
		else {
			wash = 0;
		}

		$.ajax({
            url: './add-product',
            type:'POST',
            data:{
            	productId:$(this).parents('tr').attr('data-product-id'),
            	count:$(this).parents('tr').find('span.prodCount').html(),
            	price:($(this).parents('tr').find('span.price').attr('data-product-price'))*($(this).parents('tr').find('span.prodCount').html()/($(this).parents('tr').find('span.price').attr('data-step'))),
            	wash:wash,
            	},
            dataType : 'json',
            success: function(data){
            	cartUpdate(data['cart']);	
            	labelUpdate(data);          
            }                
        }); 
	})
	$('.icon-clean').click(function(){
		$(this).toggleClass('active');
		if($(this).attr('class') != 'icon-clean' ) {
			var s_t = parseInt($('.order-big span.s-total').text().replace(/\s+/g, ''));
			$('span.s-total').text(abc(s_t+10));
		}
		else {
			var s_t = parseInt($('.order-big span.s-total').text().replace(/\s+/g, ''));
			$('span.s-total').text(abc(s_t-10));
		}
	})
	
	$('.popup table a.remove').click(function(){
		var productId = $(this).parents('tr').attr('data-product-id');
		
		//удаляем с главной
    	$('div.product-item[data-product-id="'+productId+'"] a.btn-bay').removeClass('btn-bay2');
    	$('div.product-item[data-product-id="'+productId+'"] a.btn-bay span').text('Оформить');
		$.ajax({
            url: './del-from-cart',
            type:'POST',
            data:{
            	productId:productId,            	
            	},
            dataType : 'json',
            success: function(data){  
            	$('div[data-prod-id="'+productId+'"]').hide();
            	cartUpdate(data['cart']);            	
            }                
        }); 
		$(this).parent().parent().parent().hide();
		var ju = 0;
		$('.items-table tbody tr').each(function(){
			ju+=1;
		})
		if(ju == 0) {
			$('.popup.order-big').fadeOut();
			$('#page').removeAttr('class');
			$('.popup-bg').fadeOut();
		}
		return false;
	})
	
	$('.btn-bay').on('click', function(){
		//добавляем товар в сессии		
		if( $(this).attr('class')!='btn-bay btn-bay2'){			
			$.ajax({
	            url: './add-product',
	            type:'POST',
	            data:{
	            	productId:$(this).attr('data-product-id'),
	            	count:$(this).parent('div.price-bay').find('span.prodCount').html(),
	            	price:$(this).parent('div.price-bay').find('div.pirce span.price').html(),
	            	wash:0,
	            	},
	            dataType : 'json',
	            success: function(data){    	
	            	labelUpdate(data);    
	            }                
	        }); 
        }
		if(!$('.popup.order-end').is(":visible") && $(this).attr('class')=='btn-bay btn-bay2') {
			
			/*$('.popup').css('position', 'absolute');
			$('.popup').css('top', $('.popup-bg').offset().top+10);*/
			//получаем заказы из корзины
			$.ajax({
	            url: './show-cart',
	            type:'POST',
	            data:null,
	            dataType : 'json',
	            success: function(data){ 
	            	cartUpdate(data);
	            }                
	        }); 
			$('.popup.order-big').fadeIn();
			$('.popup-bg').fadeIn();
			$('.popup-order').fadeOut();
			$('.popup-order').removeClass('fade');
			$('#page').addClass('page-g');			
			$('.popup-container').fadeIn();
			$('body').css('overflow', 'hidden');
			$('body').addClass('no-scroll');
		}	
		return false;
	})
	
	
	
	$('.order-big .btn-bay2').click(function(){
		$('.popup.order-big').fadeOut();
		$('.popup.order-end').fadeIn();
		$('input.promo').val('');
		return false;
	})
	
	$('.popup-bg').click(function(){
		$(this).fadeOut();
		$('.popup').fadeOut();
		$('.popup-container').fadeOut();
		$('#page').removeAttr('class');
		$('body').removeAttr('style');
		$('body').removeAttr('class');
	})
	
	$( "#name" ).keyup(function( event ) {
	  	var value = $(this).val();
	  	if (value != '') {
            if (!this.value.match(/^[А-Яа-я A-Za-z]+$/)) {
                $(this).addClass('error');
            }
            else {
                $(this).removeClass('error');
            }
		}
	});
	
	
	
	
	
	$( "#phone-number" ).keyup(function( event ) {
	  	var value = $(this).val();
        if (value != '') {
            if (!this.value.match(/^[0-9-()+ ]+$/)) {
                $(this).addClass('error');
            }
            else {
                $(this).removeClass('error');
            }
        }

	});
	$( ".order-data input.valid" ).blur(function() {
	  	if($(this).val() == '') {
	  		$(this).addClass('error');
	  	}
	  	else {
	  		$(this).removeClass('error');
	  	}
	});
	$( ".order-data input.valid" ).keyup(function( event ) {
	  	var value = $(this).val();
        if (value == '') {
           /* $(this).addClass('error');*/
        }
        else {
        	$(this).removeClass('error');
        }

	});
	
	$('.popup.order-end .btn-bay').click(function(){
		$( ".order-data input" ).each(function(){
			if($(this).val()==''){
				$(this).addClass('error');
			}
		})
	});
	$('.popup.order-end .btn-bay').click(function(){
		if($(this).hasClass('disabled')){
			alert('По данному адресу доставка не осуществляется!')
			return false;
		} else if($('input:not(.promo)').hasClass('error')) {
			alert('Проверьте заполненные данные!')
		}

		else{
			$('.popup.order-end').fadeOut();
			$('.popup.order-ok').fadeIn();
			
			$.ajax({
	            url: './save-order',
	            type:'POST',
	            data:$('form.orderData').serialize(),
	            dataType : 'json',
	            success: function(data){ 
	            	cartUpdate(data);
	            }                
	        }); 
		}
		return false
	})
	
	$('.btn-back').click(function(){
		$('.popup').fadeOut();		
		$('.popup-bg').fadeOut();
		$('#page').removeAttr('class');
	})
	
	$('.btn-bay').click(function(){
		if($(this).attr('class')=='btn-bay') {
			$(this).addClass('btn-bay2');
			$('span', this).text('оформить');
		}
	})
	
	
	
	$(function() {
	  $('#top-nav a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
	
	
	
	$(window).scroll(function() { 
	  var top = $('.fixes-s').offset().top,
	      pip = $('footer').offset().top;
	 	  if (top>=pip+0) {
	 	  	$('.popup-order').addClass('stop');
	 	  	$('body').css('position', 'relative');
	 	  }
	 	  else { $('.popup-order').removeClass('stop');  $('body').removeAttr('style'); }
	 	 
	});	
	
	$('body').on('input','input.promo', function(){
		$.ajax({				
            url: './check-promo',
            type:'POST',
            data:{
            	promo:$(this).val(),            	
            	},
            dataType : 'json',
            success: function(data){            	
            	if(data['disc']){
            		cartUpdate(data['cart'],data['disc']);	
            	}            	
            }                
        });
	});

	$('body').on('input','input.house, input.street', function(){
		delay(function(){      		
      		$.ajax({				
	            url: './check-address',
	            type:'POST',
	            data:{
	            	house: $('input.house').val(),            	
	            	street: $('input.street').val(),
	            	},
	            dataType : 'json',
	            success: function(data){
	            	if (data){
	            		$('div.order-end a.btn-bay2').removeClass('disabled');
	            	}else{
	            		$('div.order-end a.btn-bay2').addClass('disabled');
	            	}
	            	
	            }                
	        });
	    }, 1000 );
		
	});

	$('body').on('click', 'button.showMore', function(){
		$(this).parents('div.product-list').find('div.product-item').show();
		$(this).hide();
	});
	
})

function labelUpdate(data){
	if(data['price']>$('div#content').attr('data-free-delivery')){
		$('span.delivery').hide();	            		
	} else {
		$('span.delivery').show();	            		
	}
	$('div.popup-order div.number span').html(data.count);
	$('b#cartProdCount').html(data.count); 
	$('span.productsWord').text(declOfNum(data.count,['продукт','продукта','продуктов']));
	$('b#cartPrice').html(data.price);
}

function cartUpdate(data,disc) {   
	var allPrice = 0;
	var freeDelivery = $('div#content').attr('data-free-delivery');
	var delivery = $('div#content').attr('data-delivery');;
	var washPrice = 0;
	var orderPrice = 0
	$.each(data,function(index,val){
		orderPrice +=parseInt(val['price']);
		$('tr[data-product-id="'+index+'"]').show();
		$('tr[data-product-id="'+index+'"] span.prodCount').html(val['count']);		
		$('tr[data-product-id="'+index+'"] input.inp-val').val(val['count']);
		$('tr[data-product-id="'+index+'"] span.price').html($('tr[data-product-id="'+index+'"] span.price').attr('data-product-price')*val['count']/$('tr[data-product-id="'+index+'"] span.prodCount').attr('data-step'));
		if(val['wash']>0){
			$('tr[data-product-id="'+index+'"] i.icon-wash').addClass('active');
			$('div.summ div[data-prod-id="'+index+'"]').show();
			allPrice += parseInt(washPrice);
		}else{
			$('div.summ div[data-prod-id="'+index+'"]').hide();
		}	            		
	});
	
	if(orderPrice<freeDelivery){
		$('div.summ div.delivery').show();
		allPrice += parseInt(orderPrice) + parseInt(delivery);
		$('div.deliveryMessage').show();
	}else{
		allPrice += parseInt(orderPrice);
		$('div.deliveryMessage').hide();
		$('div.summ div.delivery').hide();
	}
	if(disc){
		var discV = 0;
		if(disc['type']='%'){			
			discV = allPrice/100*disc['val'];
		} else {
			discV = disc['val'];
		}
		allPrice = allPrice - discV;
		$('div.disc span.disc').html(discV);
		$('div.disc').show();
	}else{
		$('div.disc').hide();
	}

	//заказ на сумму
	$('div.summ span.orderPrice').html(orderPrice+'<i class="icon-rub"></i>');
	//итого
	$('div.summ span.s-total').html(allPrice);
	
}

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

function declOfNum(number, titles)  
{  
    cases = [2, 0, 1, 1, 1, 2];  
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
}  